package wifidirect.android.com.signageremote;

import android.content.BroadcastReceiver;
import android.content.Context;
import android.content.Intent;
import android.content.IntentFilter;
import android.net.wifi.ScanResult;
import android.net.wifi.WifiConfiguration;
import android.net.wifi.WifiManager;
import android.net.wifi.WpsInfo;
import android.net.wifi.p2p.WifiP2pConfig;
import android.net.wifi.p2p.WifiP2pDevice;
import android.net.wifi.p2p.WifiP2pInfo;
import android.net.wifi.p2p.WifiP2pManager;
import android.net.wifi.p2p.nsd.WifiP2pDnsSdServiceInfo;
import android.net.wifi.p2p.nsd.WifiP2pDnsSdServiceRequest;
import android.os.Handler;
import android.os.Message;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.util.Log;
import android.view.View;
import android.widget.Button;
import android.widget.EditText;
import android.widget.TextView;
import android.widget.Toast;

import java.io.IOException;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

import wifidirect.android.com.signageremote.utils.AppConstants;
import wifidirect.android.com.signageremote.wifiDirect.ChatManager;
import wifidirect.android.com.signageremote.wifiDirect.ClientSocketHandler;
import wifidirect.android.com.signageremote.wifiDirect.GroupOwnerSocketHandler;
import wifidirect.android.com.signageremote.wifiDirect.WiFiDirectBroadcastReceiver;
import wifidirect.android.com.signageremote.wifiDirect.WiFiDirectServicesList;
import wifidirect.android.com.signageremote.wifiDirect.WiFiP2pService;

public class RemoteViewActivity extends AppCompatActivity implements WiFiDirectServicesList.DeviceClickListener,
        Handler.Callback, WifiP2pManager.ConnectionInfoListener {

    private static final String TAG = "MAIN";
    private Button register, refresh;
    private ChatManager chatManager;
    private String registerString = "register", refreshString = "refresh";

    // TXT RECORD properties
    public static final String TXTRECORD_PROP_AVAILABLE = "available";
    public static final String SERVICE_INSTANCE = "_wifidemotest";
    public static final String SERVICE_REG_TYPE = "_presence._tcp";

    public static final int MESSAGE_READ = 0x400 + 1;
    public static final int MY_HANDLE = 0x400 + 2;
    private WifiP2pManager manager;

    static final int SERVER_PORT = 4545;

    private final IntentFilter intentFilter = new IntentFilter();
    private WifiP2pManager.Channel channel;
    private BroadcastReceiver receiver = null;
    private WifiP2pDnsSdServiceRequest serviceRequest;

    private Handler handler = new Handler(this);
    //    private  chatFragment;
    private WiFiDirectServicesList servicesList;

    private EditText displayName, address, assetId;
    private Button root, wifiSettings, cast, save, start, sendDetails;
    private String rootString = "root", wifiSettingsStrings = "wifiSettings", castString = "cast",
            saveString = "save", startString = "start", sendString = "sendString";

    public Handler getHandler() {
        return handler;
    }

    public void setHandler(Handler handler) {
        this.handler = handler;
    }

    String ssid;
    String pass;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_remoteview);
        register = (Button) findViewById(R.id.btnRegister);
        refresh = (Button) findViewById(R.id.btnRefresh);
        root = (Button) findViewById(R.id.btnRoot);
        cast = (Button) findViewById(R.id.toggle_screen_cast);
        save = (Button) findViewById(R.id.btnSave);
        start = (Button) findViewById(R.id.btnStart);
        sendDetails = (Button) findViewById(R.id.senddetails);
        wifiSettings = (Button) findViewById(R.id.btnWifiSetting);
        assetId = (EditText) findViewById(R.id.id_txtAssetId);
        address = (EditText) findViewById(R.id.id_txtAddress);
        displayName = (EditText) findViewById(R.id.id_txtDisplayName);

        Intent intent = getIntent();
        if (intent != null) {
            ssid = intent.getStringExtra(AppConstants.WIFI_SSID);
            pass = intent.getStringExtra(AppConstants.WIFI_PASS);
            if(ssid != null && pass != null)
            connectToWifi(ssid, pass);
        }


        root.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                if (chatManager != null) {
                    chatManager.write(rootString.getBytes());
                } else {
                    Toast.makeText(RemoteViewActivity.this, "Chat manager is null", Toast.LENGTH_LONG).show();
                }
            }
        });

        wifiSettings.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                if (chatManager != null) {
                    chatManager.write(wifiSettingsStrings.getBytes());
                } else {
                    Toast.makeText(RemoteViewActivity.this, "Chat manager is null", Toast.LENGTH_LONG).show();
                }
            }
        });

        cast.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                if (chatManager != null) {
                    chatManager.write(castString.getBytes());
                } else {
                    Toast.makeText(RemoteViewActivity.this, "Chat manager is null", Toast.LENGTH_LONG).show();
                }
            }
        });

        save.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                if (!assetId.getText().toString().isEmpty()) {
                    if (chatManager != null) {
                        chatManager.write((saveString
                                + ":" + assetId.getText().toString()).getBytes());
                    } else {
                        Toast.makeText(RemoteViewActivity.this, "Chat manager is null", Toast.LENGTH_LONG).show();
                    }
                }
            }
        });

        register.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                if (chatManager != null) {
                    chatManager.write(registerString
                            .getBytes());
                } else {
                    Toast.makeText(RemoteViewActivity.this, "Chat manager is null", Toast.LENGTH_LONG).show();
                }
            }
        });

        refresh.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                if (chatManager != null) {
                    chatManager.write(refreshString
                            .getBytes());
                } else {
                    Toast.makeText(RemoteViewActivity.this, "Chat manager is null", Toast.LENGTH_LONG).show();
                }
            }
        });

        intentFilter.addAction(WifiP2pManager.WIFI_P2P_STATE_CHANGED_ACTION);
        intentFilter.addAction(WifiP2pManager.WIFI_P2P_PEERS_CHANGED_ACTION);
        intentFilter
                .addAction(WifiP2pManager.WIFI_P2P_CONNECTION_CHANGED_ACTION);
        intentFilter
                .addAction(WifiP2pManager.WIFI_P2P_THIS_DEVICE_CHANGED_ACTION);

        manager = (WifiP2pManager) getSystemService(Context.WIFI_P2P_SERVICE);
        channel = manager.initialize(this, getMainLooper(), null);
        startRegistrationAndDiscovery();

        servicesList = new WiFiDirectServicesList();
        getFragmentManager().beginTransaction()
                .add(R.id.container_root, servicesList, "services").commit();
    }

    private void connectToWifi(String ssid, String pass) {

        WifiConfiguration conf = new WifiConfiguration();
        conf.SSID = "\"" + ssid + "\"";

        conf.preSharedKey = "\"" + pass + "\"";

        WifiManager wifiManager = (WifiManager) this.getSystemService(Context.WIFI_SERVICE);
        wifiManager.addNetwork(conf);
        int res = wifiManager.addNetwork(conf);
        wifiManager.disconnect();
        wifiManager.enableNetwork(res, true);
        wifiManager.reconnect();

    }


    public void sendDetails(View v) {

        String name, addr, asset;
        name = displayName.getText().toString();
        addr = address.getText().toString();
        asset = assetId.getText().toString();

        if (name.equalsIgnoreCase(""))
            name = "blank";
        if (addr.equalsIgnoreCase(""))
            addr = "blank";
        if (asset.equalsIgnoreCase(""))
            asset = "blank";
        if (chatManager != null) {
            chatManager.write(("Details@#$"+name + "@#$" + addr + "@#$" + asset)
                    .getBytes());
        } else {
            Toast.makeText(RemoteViewActivity.this, "Chat manager is null", Toast.LENGTH_LONG).show();
        }


    }

    private void startRegistrationAndDiscovery() {
        Map<String, String> record = new HashMap<String, String>();
        record.put(TXTRECORD_PROP_AVAILABLE, "visible");

        WifiP2pDnsSdServiceInfo service = WifiP2pDnsSdServiceInfo.newInstance(
                SERVICE_INSTANCE, SERVICE_REG_TYPE, record);
        manager.addLocalService(channel, service, new WifiP2pManager.ActionListener() {

            @Override
            public void onSuccess() {
//                appendStatus("Added Local Service");
            }

            @Override
            public void onFailure(int error) {
//                appendStatus("Failed to add a service");
            }
        });

        discoverService();

    }

    private void discoverService() {

        /*
         * Register listeners for DNS-SD services. These are callbacks invoked
         * by the system when a service is actually discovered.
         */

        manager.setDnsSdResponseListeners(channel,
                new WifiP2pManager.DnsSdServiceResponseListener() {

                    @Override
                    public void onDnsSdServiceAvailable(String instanceName,
                                                        String registrationType, WifiP2pDevice srcDevice) {

                        // A service has been discovered. Is this our app?

                        if (instanceName.equalsIgnoreCase(SERVICE_INSTANCE)) {

                            // update the UI and add the item the discovered
                            // device.
                            WiFiDirectServicesList fragment = (WiFiDirectServicesList) getFragmentManager()
                                    .findFragmentByTag("services");
                            if (fragment != null) {
                                WiFiDirectServicesList.WiFiDevicesAdapter adapter = ((WiFiDirectServicesList.WiFiDevicesAdapter) fragment
                                        .getListAdapter());
                                WiFiP2pService service = new WiFiP2pService();
                                service.device = srcDevice;
                                service.instanceName = instanceName;
                                service.serviceRegistrationType = registrationType;
                                adapter.add(service);
                                adapter.notifyDataSetChanged();
                                Log.d(TAG, "onBonjourServiceAvailable "
                                        + instanceName);
                            }
                        }

                    }
                }, new WifiP2pManager.DnsSdTxtRecordListener() {

                    /**
                     * A new TXT record is available. Pick up the advertised
                     * buddy name.
                     */
                    @Override
                    public void onDnsSdTxtRecordAvailable(
                            String fullDomainName, Map<String, String> record,
                            WifiP2pDevice device) {
                        Log.d(TAG,
                                device.deviceName + " is "
                                        + record.get(TXTRECORD_PROP_AVAILABLE));
                    }
                });

        // After attaching listeners, create a service request and initiate
        // discovery.
        serviceRequest = WifiP2pDnsSdServiceRequest.newInstance();
        manager.addServiceRequest(channel, serviceRequest,
                new WifiP2pManager.ActionListener() {

                    @Override
                    public void onSuccess() {
//                        appendStatus("Added service discovery request");
                    }

                    @Override
                    public void onFailure(int arg0) {
//                        appendStatus("Failed adding service discovery request");
                    }
                });
        manager.discoverServices(channel, new WifiP2pManager.ActionListener() {

            @Override
            public void onSuccess() {
//                appendStatus("Service discovery initiated");
            }

            @Override
            public void onFailure(int arg0) {
//                appendStatus("Service discovery failed");

            }
        });
    }

    @Override
    public void connectP2p(WiFiP2pService service) {
        WifiP2pConfig config = new WifiP2pConfig();
        config.deviceAddress = service.device.deviceAddress;
        config.wps.setup = WpsInfo.PBC;
        if (serviceRequest != null)
            manager.removeServiceRequest(channel, serviceRequest,
                    new WifiP2pManager.ActionListener() {

                        @Override
                        public void onSuccess() {
                        }

                        @Override
                        public void onFailure(int arg0) {
                        }
                    });

        manager.connect(channel, config, new WifiP2pManager.ActionListener() {

            @Override
            public void onSuccess() {
//                appendStatus("Connecting to service");


            }

            @Override
            public void onFailure(int errorCode) {
//                appendStatus("Failed connecting to service");
            }
        });
    }

    @Override
    public boolean handleMessage(Message msg) {
        switch (msg.what) {
            case MESSAGE_READ:
                byte[] readBuf = (byte[]) msg.obj;
                // construct a string from the valid bytes in the buffer
                String readMessage = new String(readBuf, 0, msg.arg1);
                Log.d(TAG, readMessage);
//                (chatFragment).pushMessage("Buddy: " + readMessage);
                break;

            case MY_HANDLE:
                Object obj = msg.obj;
//                (chatFragment).setChatManager((ChatManager) obj);
                chatManager = (ChatManager) obj;

        }
        return true;
    }

    @Override
    public void onResume() {
        super.onResume();
        receiver = new WiFiDirectBroadcastReceiver(manager, channel, this);
        registerReceiver(receiver, intentFilter);
    }

    @Override
    public void onPause() {
        super.onPause();
        unregisterReceiver(receiver);
    }

    @Override
    public void onConnectionInfoAvailable(WifiP2pInfo p2pInfo) {
        Thread handler = null;
        /*
         * The group owner accepts connections using a server socket and then spawns a
         * client socket for every client. This is handled by {@code
         * GroupOwnerSocketHandler}
         */

        if (p2pInfo.isGroupOwner) {
            Log.d(TAG, "Connected as group owner");
            try {
                handler = new GroupOwnerSocketHandler(
                        (this).getHandler());
                handler.start();
            } catch (IOException e) {
                Log.d(TAG,
                        "Failed to create a server thread - " + e.getMessage());
                return;
            }
        } else {
            Log.d(TAG, "Connected as peer");
            handler = new ClientSocketHandler(
                    (this).getHandler(),
                    p2pInfo.groupOwnerAddress);
            handler.start();
        }
//        chatFragment = new WiFiChatFragment();
//        getFragmentManager().beginTransaction()
//                .replace(R.id.container_root, chatFragment).commit();
//        statusTxtView.setVisibility(View.GONE);
    }

}
