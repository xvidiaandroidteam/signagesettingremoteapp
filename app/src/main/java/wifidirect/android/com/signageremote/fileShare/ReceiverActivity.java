package wifidirect.android.com.signageremote.fileShare;

import android.content.BroadcastReceiver;
import android.content.Context;
import android.content.Intent;
import android.content.IntentFilter;
import android.net.wifi.ScanResult;
import android.net.wifi.WifiConfiguration;
import android.net.wifi.WifiManager;
import android.os.Bundle;
import android.os.Environment;
import android.os.Handler;
import android.os.Looper;
import android.os.Message;
import android.support.v7.app.AppCompatActivity;
import android.util.Log;
import android.view.View;
import android.widget.TextView;
import android.widget.Toast;

import java.io.BufferedReader;
import java.io.File;
import java.io.FileInputStream;
import java.io.InputStreamReader;
import java.util.List;

import io.github.karuppiah7890.fileshare.FileReceiver;
import io.github.karuppiah7890.fileshare.utils.AppState;
import wifidirect.android.com.signageremote.R;

public class ReceiverActivity extends AppCompatActivity {

    FileReceiver fileReceiver;
    TextView tvCode;
    int size = 0;
    WifiManager wifi;
    List<ScanResult> results;
    String ssid;
    String passkey;
    BroadcastReceiver wifiBroadcastReceiver;

    private final Handler mHandler = new Handler(Looper.getMainLooper()) {
        @Override
        public void handleMessage(Message msg) {
            switch (msg.what) {
                case FileReceiver.CODE:
                    tvCode.setText((int) msg.obj + "");
                    break;

                case FileReceiver.LISTENING:
                    Toast.makeText(ReceiverActivity.this, "Listening...", Toast.LENGTH_SHORT).show();
                    break;

                case FileReceiver.CONNECTED:
                    Toast.makeText(ReceiverActivity.this, "Connected!", Toast.LENGTH_SHORT).show();
                    break;

                case FileReceiver.RECEIVING_FILE:
                    Toast.makeText(ReceiverActivity.this, "Receiving File!", Toast.LENGTH_SHORT).show();
                    break;

                case FileReceiver.FILE_RECEIVED:
                    if (ApManager.isApOn(ReceiverActivity.this)) {
                        ApManager.configApState(ReceiverActivity.this);
                    }
                    File file = (File) msg.obj;
                    Toast.makeText(ReceiverActivity.this, file.getName() + " Received!", Toast.LENGTH_SHORT).show();
                    Toast.makeText(ReceiverActivity.this, "Stored in " + file.getAbsolutePath(), Toast.LENGTH_SHORT).show();
                    fileReceiver.close();
//                    if (ApManager.isApOn(ReceiverActivity.this)) {
//                        ApManager.configApState(ReceiverActivity.this);
//                    }
                    if (wifi.isWifiEnabled() == false) {
                        Toast.makeText(getApplicationContext(), "wifi is disabled..making it enabled", Toast.LENGTH_LONG).show();
                        wifi.setWifiEnabled(true);

                    }
                    String details = readTextFromFile(file.getAbsolutePath());
                    final String[] split1 = details.split(":");
                    final String networkSSID = "", networkPASS = "";
                    /*if (split1.length > 1) {
                        networkSSID = split1[0];
                        networkPASS = split1[1];
                    }*/
                    new Handler().postDelayed(new Runnable() {
                        @Override
                        public void run() {
                            connectToWifi(split1[0], split1[1]);
                        }
                    },5000);

                    Log.e("details", details);

                    break;

                case FileReceiver.RECEIVE_ERROR:
                    Toast.makeText(ReceiverActivity.this, "Error occured : " + (String) msg.obj, Toast.LENGTH_SHORT).show();
                    fileReceiver.close();
                    break;
            }
        }
    };

    private void connectToWifi(String ssid, String pass) {

        WifiConfiguration conf = new WifiConfiguration();
        conf.SSID = "\"" + ssid + "\"";

        conf.preSharedKey = "\"" + pass + "\"";

        WifiManager wifiManager = (WifiManager) this.getSystemService(Context.WIFI_SERVICE);
        wifiManager.addNetwork(conf);

        List<WifiConfiguration> list = wifiManager.getConfiguredNetworks();
        for (ScanResult i : results) {
            if (i.SSID != null && i.SSID.equals("Xvidia_opss")) {
                int res = wifiManager.addNetwork(conf);
                wifiManager.disconnect();
                wifiManager.enableNetwork(res, true);
                wifiManager.reconnect();
                break;
            }
        }

    }

    private String readTextFromFile(String path) {
        String retMesg = "";
        try {
//			File myFile = new File("/sdcard/mysdfile.txt");
//            String destPath = Environment.getExternalStorageDirectory()
//                    + AppState.FILE_FOLDER;
            File myFile = new File(path);
            if (myFile.exists()) {
                FileInputStream fIn = new FileInputStream(myFile);
                BufferedReader myReader = new BufferedReader(
                        new InputStreamReader(fIn));
                String aDataRow = "";
                String aBuffer = "";
                while ((aDataRow = myReader.readLine()) != null) {
                    aBuffer += aDataRow;
                }
                myReader.close();
                retMesg = aBuffer;
            }
        } catch (Exception e) {
        }
        return retMesg;
    }

    public void getFile(View view) {

        fileReceiver = new FileReceiver(this,mHandler);

        fileReceiver.getFile();
//        connectToWifi("", "");
    }

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_receiver);

        tvCode = (TextView) findViewById(R.id.tvCode);

        wifi = (WifiManager) getSystemService(Context.WIFI_SERVICE);
        wifi.startScan();
        if (!ApManager.isApOn(ReceiverActivity.this)) {
            ApManager.configApState(ReceiverActivity.this);
        }
        String destPath = Environment.getExternalStorageDirectory() + "/FileSharer" + AppState.FILE_NAME_Log;
        File logFile = new File(destPath);
        if (logFile.exists())
            logFile.delete();

        registerReceiver(new BroadcastReceiver() {
            @Override
            public void onReceive(Context c, Intent intent) {
                results = wifi.getScanResults();
                size = results.size();
            }
        }, new IntentFilter(WifiManager.SCAN_RESULTS_AVAILABLE_ACTION));
    }

    @Override
    protected void onPause() {
        super.onPause();
        if (wifiBroadcastReceiver != null) {
            unregisterReceiver(wifiBroadcastReceiver);
        }
    }
}
