package wifidirect.android.com.signageremote.fileShare;

import android.app.Activity;
import android.content.Intent;
import android.os.Bundle;
import android.os.Environment;
import android.os.Handler;
import android.os.Looper;
import android.os.Message;
import android.support.v7.app.AppCompatActivity;
import android.support.v7.widget.Toolbar;
import android.util.Log;
import android.view.View;
import android.widget.Button;
import android.widget.EditText;
import android.widget.Toast;

import java.io.BufferedWriter;
import java.io.File;
import java.io.FileWriter;
import java.io.IOException;
import java.util.ArrayList;

import droidninja.filepicker.FilePickerConst;
import io.github.karuppiah7890.fileshare.FileSender;
import io.github.karuppiah7890.fileshare.utils.AppState;
import wifidirect.android.com.signageremote.RemoteViewActivity;
import wifidirect.android.com.signageremote.R;
import wifidirect.android.com.signageremote.utils.AppConstants;

public class SenderActivity extends AppCompatActivity {

    private ArrayList<String> docPaths;
    private EditText etCode;
    private FileSender fileSender;
    Button bPickFile;
    private String ssid, pass;
    private Toolbar toolbar;

    private final Handler mHandler = new Handler(Looper.getMainLooper()) {
        @Override
        public void handleMessage(Message msg) {
            switch (msg.what) {
                case FileSender.CONNECTING :
                    Toast.makeText(SenderActivity.this,"Connecting...",Toast.LENGTH_SHORT).show();
                    break;

                case FileSender.CONNECTED :
                    Toast.makeText(SenderActivity.this,"Connected!",Toast.LENGTH_SHORT).show();
                    break;

                case FileSender.SENDING_FILE :
                    Toast.makeText(SenderActivity.this,"Sending File!",Toast.LENGTH_SHORT).show();
                    break;

                case FileSender.FILE_SENT :
                    Toast.makeText(SenderActivity.this,"Details Sent! wait...",Toast.LENGTH_SHORT).show();
                    fileSender.close();
                    bPickFile.setEnabled(true);

                    new Handler().postDelayed(new Runnable() {
                        @Override
                        public void run() {
                            Intent intent = new Intent(SenderActivity.this, RemoteViewActivity.class);
                            Bundle bundle = new Bundle();
                            bundle.putString(AppConstants.WIFI_SSID, ssid);
                            bundle.putString(AppConstants.WIFI_PASS, pass);
                            intent.putExtras(bundle);
                            startActivity(intent);
                        }
                    },1000);


                    break;

                case FileSender.SEND_ERROR :
                    Toast.makeText(SenderActivity.this,"Error occured : " + (String)msg.obj,Toast.LENGTH_SHORT).show();
                    fileSender.close();
                    bPickFile.setEnabled(true);
                    break;
            }
        }
    };

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_sender);

        toolbar = (Toolbar) findViewById(R.id.toolbar);
        setSupportActionBar(toolbar);
        toolbar.setPadding(0, 0, 0, 0);
        toolbar.setTitle("Enter code");

        Intent intent = getIntent();
        if(intent != null){
            ssid = intent.getStringExtra(AppConstants.WIFI_SSID);
            pass = intent.getStringExtra(AppConstants.WIFI_PASS);
        }

        etCode = (EditText) findViewById(R.id.etCode);
        bPickFile = (Button) findViewById(R.id.bPickFile);
    }

    private void sendMyFile(String filePath) {

        Log.i("SenderActivity","File Path : " + filePath);
        Toast.makeText(this,filePath,Toast.LENGTH_SHORT).show();

        File file = new File(filePath);

        fileSender = new FileSender(this,mHandler);

        if(!etCode.getText().toString().equals("")){
            int code = Integer.parseInt(etCode.getText().toString());
            fileSender.sendFile(file,code);
        }


    }

    public void pickAFile(View view){
        if(!ssid.equalsIgnoreCase("") && !pass.equalsIgnoreCase("")) {
            String string = ssid + ":" + pass;
            writeLogFile(string);
        }
        /*FilePickerBuilder.getInstance().setMaxCount(1)
                .setActivityTheme(R.style.AppTheme)
                .pickDocument(this);

        view.setEnabled(false);*/
    }

    public void writeLogFile(String string){
        String destPath = Environment.getExternalStorageDirectory() + AppState.FILE_NAME_Log;
        File logFile = new File(destPath);

        try {
            if(logFile.exists())
                logFile.delete();
            logFile.createNewFile();
        } catch (IOException e) {
            e.printStackTrace();
        }
        BufferedWriter buf=null;

        try
        {
            //BufferedWriter for performance, true to set append to file flag
            buf = new BufferedWriter(new FileWriter(logFile, true));
//				String time = "========"+LogUtility.getTimeStamp()+"==============";
//				buf.append(time);
				buf.append(string);
//				buf.newLine();
        }
        catch (IOException e)
        {
            // TODO Auto-generated catch block
            e.printStackTrace();
        }finally {
            if(buf!= null) {
                try {
                    buf.flush();
                    buf.close();
                    sendMyFile(destPath);
                } catch (IOException e) {
                    e.printStackTrace();
                }
            }
        }
    }

    @Override
    public void onActivityResult(int requestCode, int resultCode, Intent data) {
        switch (requestCode)
        {
            case FilePickerConst.REQUEST_CODE_DOC:
                if(resultCode== Activity.RESULT_OK && data!=null)
                {
                    docPaths = new ArrayList<>();
                    docPaths.addAll(data.getStringArrayListExtra(FilePickerConst.KEY_SELECTED_DOCS));
                    sendMyFile(docPaths.get(0));
                }
                break;
        }
    }
}
