package io.github.karuppiah7890.fileshare.utils;

/**
 * This class has static fields for app.
 * @author Ravi@Xvidia
 * @since version 1.0
 *
 */
public class AppState {

	public static final String FILE_FOLDER= "/FileShare/";

	public static final String FILE_NAME_Log= "/connectiondetails.txt";
	public static final String DISPLAY_FOLDER= "/Android/data/in.com.app.release/Xvidia/Display/";

	public static final String DOWNLOAD_FOLDER= "/Android/data/in.com.app.release/Xvidia/Download/";
}
