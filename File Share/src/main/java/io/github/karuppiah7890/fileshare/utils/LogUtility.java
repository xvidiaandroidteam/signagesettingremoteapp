package io.github.karuppiah7890.fileshare.utils;

import android.content.Context;
import android.net.ConnectivityManager;
import android.net.NetworkInfo;
import android.os.Environment;

import java.io.BufferedReader;
import java.io.File;
import java.io.FileInputStream;
import java.io.FileReader;
import java.io.IOException;
import java.io.InputStream;
import java.io.InputStreamReader;
import java.text.SimpleDateFormat;
import java.util.Date;
import java.util.Locale;

/**
 * This is a utility class
 * 
 * @author Ravi@xvidia
 * 
 */
public class LogUtility {

//	static String FILE_NAME_ASSET = "assetId.txt";
//	static String FILE_NAME_DISPLAY = "display.txt";
//	static JSONArray object = new JSONArray();
//	JSONObject heartBeatObject = new JSONObject();
//	JSONObject activeLayoutObject = new JSONObject();
//	JSONObject appStatusObject = new JSONObject();
//	JSONObject appVersionObject = new JSONObject();
//	JSONObject locationObject = new JSONObject();
//	JSONObject newtorkObject = new JSONObject();
//	JSONObject screenResolutionObject = new JSONObject();
	private static String timeStamp;

	public static boolean isUpdateCheck() {
		return updateCheck;
	}

	public static void setUpdateCheck(boolean updateCheck) {
		LogUtility.updateCheck = updateCheck;
	}

	private static boolean updateCheck = false;
//	static String offTimeId; 
	private static  LogUtility instance = null;

	public static  LogUtility getInstance() {
		if (instance == null) {
			instance = new LogUtility();
		}
		return instance;
	}
	
	/**
	 * This method replaces substring between two strings
	 * 
	 * @param builder StringBuilder builder
	 * @param from string
	 * @param to string
	 * @return
	 * @deprecated
	 * @since version1.0
	 */
	public static String replaceAll(StringBuilder builder, String from,
			String to) {
		int index = builder.indexOf(from);
		while (index != -1) {
			builder.replace(index, index + from.length(), to);
			index += to.length(); // Move to the end of the replacement
			index = builder.indexOf(from, index);
		}
		return builder.toString();
	}

	/**
	 * This method write JSONObject
	 * 
	 * @param time variable for time string
	 * @param tag JSon Tag
	 * @param value JSon value
	 * @since version1.0
	 */
	/*public static void writeJSON(String time, String tag, String value) {
		JSONObject object = new JSONObject();
		try {
			object.put(LogData.TAG_TIME, time);
			object.put(tag, value);
		} catch (JSONException e) {
			// e.printStackTrace();
		}
		System.out.println(object);
		writeJSONArray(object);
	}*/

	/**
	 * This method gets the JSON for the {@link LogUtility}
	 *
	 * @return JSONObject of {@link LogUtility}
	 * @since version1.0
	 */
	/**
	 * This method gets the JSON for the {@link LogUtility}
	 * 
	 * @return JSONObject of {@link LogUtility}
	 * @since version1.0
	 */
	/*public JSONObject getHeartBeatJSON(Context ctx) {
		heartBeatObject = new JSONObject();
		try {
//			getFileInfo();
			heartBeatObject.put(LogData.TAG_APPID, LogData.getInstance().getAppID(ctx));
			heartBeatObject.put(LogData.TAG_TIME, getTimeStamp());
			heartBeatObject.put(LogData.TAG_ASSETID, LogData.getInstance().getAssetId(ctx));
			heartBeatObject.put(LogData.TAG_DisplayName, LogData.getInstance().getDisplayName(ctx));
		} catch (JSONException e) {
			 e.printStackTrace();
		} catch (Exception e) {
			e.printStackTrace();
		}
		if (heartBeatObject == null) {
			heartBeatObject = new JSONObject();
		}

		return heartBeatObject;
	}

	public JSONObject getActiveLayoutJSON(Context ctx) {
		activeLayoutObject = new JSONObject();

		try {
			activeLayoutObject.put(LogData.TAG_APPID, LogData.getInstance().getAppID(ctx));
			activeLayoutObject.put(LogData.TAG_TIME,getTimeStamp());
			activeLayoutObject.put(LogData.TAG_ASSETID, LogData.getInstance().getAssetId(ctx));
			activeLayoutObject.put(LogData.TAG_DisplayName, LogData.getInstance().getDisplayName(ctx));
			activeLayoutObject.put(LogData.TAG_PREV_LAYOUT, LogData.getInstance().getPrevLayout(ctx));
			activeLayoutObject.put(LogData.TAG_ACTIVE_LAYOUT, LogData.getInstance().getActiveLayout(ctx));
		} catch (JSONException e) {
			// e.printStackTrace();
		} catch (Exception e) {

		}
		if (activeLayoutObject == null) {
			activeLayoutObject = new JSONObject();
		}

		return activeLayoutObject;
	}
	
	public  JSONObject getAppStatusJSON(Context ctx) {
		appStatusObject = new JSONObject();

		try {
			appStatusObject.put(LogData.TAG_APPID, LogData.getInstance().getAppID(ctx));
			appStatusObject.put(LogData.TAG_TIME,getTimeStamp());
			appStatusObject.put(LogData.TAG_ASSETID, LogData.getInstance().getAssetId(ctx));
			appStatusObject.put(LogData.TAG_DisplayName, LogData.getInstance().getDisplayName(ctx));
			appStatusObject.put(LogData.TAG_PREV_APP_STATUS, LogData.getInstance().getPrevAppStatus(ctx));
			appStatusObject.put(LogData.TAG_APPLICATION_STATUS, LogData.getInstance().getAppStatus(ctx));
		} catch (JSONException e) {
			// e.printStackTrace();
		} catch (Exception e) {

		}
		if (appStatusObject == null) {
			appStatusObject = new JSONObject();
		}

		return appStatusObject;
	}
	
	public  JSONObject getLocationJson(Context ctx) {
		locationObject = new JSONObject();

		try {
			locationObject.put(LogData.TAG_APPID, LogData.getInstance().getAppID(ctx));
			locationObject.put(LogData.TAG_TIME,getTimeStamp());
			locationObject.put(LogData.TAG_ASSETID, LogData.getInstance().getAssetId(ctx));
			locationObject.put(LogData.TAG_DisplayName, LogData.getInstance().getDisplayName(ctx));
			locationObject.put(LogData.TAG_PREV_LOCATION, LogData.getInstance().getPrevLocation(ctx));
			locationObject.put(LogData.TAG_LOCATION, LogData.getInstance().getLocation(ctx));
		} catch (JSONException e) {
			// e.printStackTrace();
		} catch (Exception e) {

		}
		if (locationObject == null) {
			locationObject = new JSONObject();
		}

		return locationObject;
	}
	public  JSONObject getAppVersionJson(Context ctx) {
		appVersionObject = new JSONObject();

		try {
			appVersionObject.put(LogData.TAG_APPID, LogData.getInstance().getAppID(ctx));
			appVersionObject.put(LogData.TAG_TIME,getTimeStamp());
			appVersionObject.put(LogData.TAG_ASSETID, LogData.getInstance().getAssetId(ctx));
			appVersionObject.put(LogData.TAG_DisplayName, LogData.getInstance().getDisplayName(ctx));
			appVersionObject.put(LogData.TAG_PREV_LOCATION, LogData.getInstance().getPrevAppVersion(ctx));
			appVersionObject.put(LogData.TAG_LOCATION, LogData.getInstance().getAppVersion(ctx));
		} catch (JSONException e) {
			// e.printStackTrace();
		} catch (Exception e) {

		}
		if (appVersionObject == null) {
			appVersionObject = new JSONObject();
		}

		return appVersionObject;
	}
	public  JSONObject getNetworkJson(Context ctx) {
		newtorkObject = new JSONObject();

		try {
			newtorkObject.put(LogData.TAG_APPID, LogData.getInstance().getAppID(ctx));
			newtorkObject.put(LogData.TAG_TIME,getTimeStamp());
			newtorkObject.put(LogData.TAG_ASSETID, LogData.getInstance().getAssetId(ctx));
			newtorkObject.put(LogData.TAG_DisplayName, LogData.getInstance().getDisplayName(ctx));
			newtorkObject.put(LogData.TAG_PREV_IPADDRESS, LogData.getInstance().getPrevIpAddress(ctx));
			newtorkObject.put(LogData.TAG_IPADDRESS, LogData.getInstance().getIpAddress(ctx));
			newtorkObject.put(LogData.TAG_PREV_MAC_ADDRESS, LogData.getInstance().getPrevMacAddress(ctx));
			newtorkObject.put(LogData.TAG_MAC_ADDRESS, LogData.getInstance().getMacAddress(ctx));
			newtorkObject.put(LogData.TAG_PREV_NETWORK_TYPE, LogData.getInstance().getPrevNetworkType(ctx));
			newtorkObject.put(LogData.TAG_NETWORKTYPE, LogData.getInstance().getNetworkType(ctx));
		} catch (JSONException e) {
			// e.printStackTrace();
		} catch (Exception e) {

		}
		if (newtorkObject == null) {
			newtorkObject = new JSONObject();
		}

		return newtorkObject;
	}
	
	public  JSONObject getScreenReolutionJson(Context ctx) {
		screenResolutionObject = new JSONObject();

		try {
			screenResolutionObject.put(LogData.TAG_APPID, LogData.getInstance().getAppID(ctx));
			screenResolutionObject.put(LogData.TAG_TIME,getTimeStamp());
			screenResolutionObject.put(LogData.TAG_ASSETID, LogData.getInstance().getAssetId(ctx));
			screenResolutionObject.put(LogData.TAG_DisplayName, LogData.getInstance().getDisplayName(ctx));
			screenResolutionObject.put(LogData.TAG_PREV_SCREEN_RESOLUTION, LogData.getInstance().getPrevScreenResolution(ctx));
			screenResolutionObject.put(LogData.TAG_SCREEN_RESOLUTION, LogData.getInstance().getScreenResolution(ctx));
		} catch (JSONException e) {
			// e.printStackTrace();
		} catch (Exception e) {

		}
		if (screenResolutionObject == null) {
			screenResolutionObject = new JSONObject();
		}

		return screenResolutionObject;
	}*/
	/**
	 * This method sets the JSON for the {@link LogUtility }
	 * 
	 * @since version1.0
	 */
	// static void setHeartBeatJSON() {
	// heartBeatObject = new JSONObject();
	// try {
	// getFileInfo();
	// heartBeatObject.put(TAG_TIME,
	// HeartBeatData.getInstance().getTimeStamp());
	// heartBeatObject.put(TAG_ASSETID, getAssetId());
	// heartBeatObject.put(TAG_DisplayName, getDisplayName());
	// heartBeatObject.put(TAG_SCREEN_RESOLUTION, getResolution());
	// heartBeatObject.put(TAG_ADDRESS, getAddress());
	// heartBeatObject.put(TAG_APPLICATION_STATUS, getAppStatus());
	// heartBeatObject.put(TAG_NETWORKTYPE,
	// HeartBeatData.getInstance().getneworkType());
	// heartBeatObject.put(TAG_ACTIVE_LAYOUT,
	// HeartBeatData.getInstance().getCurrenLayout());
	// heartBeatObject.put(TAG_IPADDRESS,
	// HeartBeatData.getInstance().getIpAddress());
	// heartBeatObject.put(TAG_MAC_ADDRESS,
	// HeartBeatData.getInstance().getMacAddress());
	// heartBeatObject.put(TAG_LOCATION,
	// HeartBeatData.getInstance().getLocation());
	// heartBeatObject.put(TAG_GEOPIP,
	// HeartBeatData.getInstance().getLocation());
	// heartBeatObject.put(TAG_APPVERSION, getAppVersion());
	// heartBeatObject.put(TAG_APPID, getAppID());
	// Log.i("HEARTBEAT value", ""+heartBeatObject.toString());
	// } catch (JSONException e) {
	// // e.printStackTrace();
	// }
	// }
	
	public static String getTimeStamp() {
		setTimeStamp();
		return timeStamp;
	}

	 private static void setTimeStamp() {
		SimpleDateFormat formatter = new SimpleDateFormat(
				"dd-MM-yyyy HH:mm:ss",Locale.ENGLISH);
		Date now = new Date();
		timeStamp = "" + formatter.format(now);

	}
	
//	 public static String getOffTimeId() {
//			return offTimeId;
//		}
//
//		public static void setOffTimeId(String id) {
//			offTimeId = id;
//
//		}

//	private static void writeJSONArray(JSONObject obj) {
//		object.put(obj);
////		System.out.println(object);
//	}

	

	static String readTextFromFile(String filename) {
		String retMesg = "";
		try {
			// File myFile = new File("/sdcard/mysdfile.txt");
			String destPath = Environment.getExternalStorageDirectory()
					+ AppState.FILE_FOLDER;
			File myFile = new File(destPath + filename);
			if (myFile.exists()) {
				FileInputStream fIn = new FileInputStream(myFile);
				BufferedReader myReader = new BufferedReader(
						new InputStreamReader(fIn));
				String aDataRow = "";
				String aBuffer = "";
				while ((aDataRow = myReader.readLine()) != null) {
					aBuffer += aDataRow;
				}
				myReader.close();
				retMesg = aBuffer;
			}
		} catch (Exception e) {
		}
		return retMesg;
	}


	/**
	 * This method checks if networkis connected or not
	 * 
	 * @param context
	 * @return true is connected to internet else false
	 */
	public static boolean checkNetwork(Context context) {
		boolean nwFlag = false;
		try {
			ConnectivityManager connMgr = (ConnectivityManager) context
					.getSystemService(Context.CONNECTIVITY_SERVICE);
			NetworkInfo networkInfo = connMgr.getActiveNetworkInfo();
			if (networkInfo != null && networkInfo.isConnected()) {
				nwFlag = true;
			}
		} catch (Exception e) {
		}

		return nwFlag;
	}
	public static String getLayoutStringFromFile(String fileName){

		String PATH = Environment.getExternalStorageDirectory()
				+ AppState.DISPLAY_FOLDER;
		File file = new File(PATH, fileName);

		//Read text from file
		StringBuilder text = new StringBuilder();

		try {
			BufferedReader br = new BufferedReader(new FileReader(file));
			String line;

			while ((line = br.readLine()) != null) {
				text.append(line);
			}
			br.close();
		}
		catch (IOException e) {
			e.printStackTrace();
		}catch (Exception e) {
				e.printStackTrace();
			}
		
		String layoutString = "";
		if(text != null ){
			layoutString = new String(text);
		}
		if(!layoutString.contains("layout")){
			layoutString = "";
		}
		return layoutString;
	}
	/**
	 * This method checks if networkis connected or not
	 *
	 * @param context
	 * @return true is connected to internet else false
	 */
	public static boolean checkNetwrk(Context context) {
		boolean nwFlag = false;
		try {
			ConnectivityManager connMgr = (ConnectivityManager) context
					.getSystemService(Context.CONNECTIVITY_SERVICE);
			NetworkInfo networkInfo = connMgr.getActiveNetworkInfo();
			if (networkInfo != null && networkInfo.isConnected()) {
				nwFlag = true;
			}
		} catch (Exception e) {
//			 e.printStackTrace();
		}

		return nwFlag;
	}

}
